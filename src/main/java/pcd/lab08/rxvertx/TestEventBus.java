package pcd.lab08.rxvertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;

public class TestEventBus {

	static class MyVerticle extends AbstractVerticle {
		
		private String channel;
		
		public MyVerticle(String channel) {
			this.channel = channel;
		}
		  public void start() {
			EventBus eb = vertx.eventBus();
			eb.consumer(channel, event -> {
				System.out.println("perceived: "+ event);
			});
		  }
	}	
	
	public static void main(String[] args) throws Exception  {
		Vertx  vertx = Vertx.vertx();
		
		String channelName = "an-event-channel";
		
		MyVerticle myVerticle = new MyVerticle(channelName);
		vertx.deployVerticle(myVerticle);
		
		Thread.sleep(1000);
		EventBus eb = vertx.eventBus();
		eb.publish("an-event-channel", "MSG #1");
		Thread.sleep(1000);
		eb.publish("an-event-channel", "MSG #2");
	}

}
