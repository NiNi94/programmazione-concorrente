package pcd.lab08.rx;

import java.util.stream.IntStream;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class Test03d_create_hot_pubsub {

	public static void main(String[] args) throws Exception {

		log("creating hot observable.");

		PublishSubject<Integer> source = PublishSubject.<Integer>create();
		 
		log("subscribing.");

		source.subscribe(Test03d_create_hot_pubsub::compute, Throwable::printStackTrace);
		 
		log("generating.");

		IntStream.range(1, 10).forEach(source::onNext);

		log("waiting.");

		Thread.sleep(1000);
	}
	
	public static void compute(Integer v) {
        try {
            log("compute integer v: " + v);
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
	
	static private void log(String msg) {
		System.out.println("[" + Thread.currentThread() + "] " + msg);
	}
	

}
