package pcd.lab08.rx;

import java.util.concurrent.TimeUnit;

import rx.Observable;

public class Test06_time_flow {

	public static void main(String[] args) throws Exception {

		long startTime = System.currentTimeMillis(); 
		
		log("Generating.");
		Observable
        		.interval(100, TimeUnit.MILLISECONDS)
        		.timestamp()
        		.sample(500, TimeUnit.MILLISECONDS)
        		.map(ts -> ts.getTimestampMillis() - startTime + "ms: " + ts.getValue())
        		.take(10)
        		.subscribe(Test06_time_flow::log);
		
		log("Going to sleep.");
		Thread.sleep(10000);
		log("Done.");
		
	}

	static private void log(String msg) {
		System.out.println("[" + Thread.currentThread() + "] " + msg);
	}
	
}
