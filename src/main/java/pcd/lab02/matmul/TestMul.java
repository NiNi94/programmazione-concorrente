package pcd.lab02.matmul;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class TestMul {

	public static void main(String[] args) throws Exception {
		
		/* square matrix */
		int size = 1000;

		
		int sizeFirstMatrix = size; /* num rows mat A */
		int sizeSecondMatrix = size; /* num columns mat A = num rows mat B */
		int sizeResultMatrix = size; /* nun columns mat B */
		boolean debugging = false;

		System.out.println("Testing A["+sizeFirstMatrix+","+sizeSecondMatrix+"]*B["+sizeSecondMatrix+","+sizeResultMatrix+"]");
		System.out.println("Initialising...");

		Mat matA = new Mat(sizeFirstMatrix, sizeResultMatrix);
		matA.initRandom(10);

		if (debugging) {
			System.out.println("A:");
			matA.print();
		}
		
		Mat matB = new Mat(sizeResultMatrix,sizeSecondMatrix);
		matB.initRandom(2);
		
		if (debugging){
			System.out.println("B:");
			matB.print();
		}
		
		System.out.println("Initialising done.");
		System.out.println("Computing matmul...");

		Chrono cron = new Chrono();
		cron.start();
		List<Thread> threadList = new ArrayList<>();
		MatMultithread matClass = new MatMultithread(250,matA ,matB);
		IntStream.range(0,250).forEach(t -> threadList.add(new Thread(() -> matClass.multiThreadMultiplication())));

		threadList.stream().forEach(t -> {
				t.start();
			System.out.println(t.getName());
		});
		threadList.stream().forEach(t -> {
			try {
				t.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		});
		//Mat matC = Mat.mul(matA, matB);
		cron.stop();

		Mat matC = matClass.getResultMatrix();
		System.out.println("Computing matmul done.");

		if (debugging){
			System.out.println("C:");
			matC.print();
		}
		
		System.out.println("Time elapsed: "+cron.getTime()+" ms.");
		
	}



}
