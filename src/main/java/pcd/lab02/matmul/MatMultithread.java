package pcd.lab02.matmul;

public class MatMultithread {
    private Mat firstMatrix;
    private Mat secondMatrix;
    private Mat resultMatrix;
    private int numberOfThread;
    private int numberOfRowsPerThread;
    private int workingThreads;


    public MatMultithread(int numberOfThread, Mat firstMatrix, Mat secondMatrix) {
        this.numberOfThread = numberOfThread;
        this.firstMatrix = firstMatrix;
        this.secondMatrix = secondMatrix;
        this.resultMatrix = new Mat(firstMatrix.getNRows(), secondMatrix.getNColumns());
        this.numberOfRowsPerThread = firstMatrix.getNRows()/this.numberOfThread;
        this.workingThreads = -1;
    }

    public void multiThreadMultiplication() {
        this.incCalls();
        int startIndexRow = this.numberOfRowsPerThread * this.workingThreads;
        int endIndexRow =  startIndexRow + this.numberOfRowsPerThread;

        for (int i = startIndexRow; i < endIndexRow; i++) {
            for (int j = 0; j < this.resultMatrix.getNColumns(); j++) {
                double sum = 0;
                for (int k = 0; k < this.secondMatrix.getNRows(); k++) {
                    sum += this.firstMatrix.get(i, k) * this.secondMatrix.get(k, j);
                }
                this.resultMatrix.set(i, j, sum);
            }
        }
    }

    public synchronized void incCalls() {
        this.workingThreads++;
    }
    public Mat getResultMatrix() {
        return resultMatrix;
    }
}
