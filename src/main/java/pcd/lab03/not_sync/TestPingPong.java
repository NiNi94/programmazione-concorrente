package pcd.lab03.not_sync;

/**
 * Unsynchronized version
 * 
 * @TODO make it sync - i.e. 
 * @author aricci
 *
 */
public class TestPingPong {

	public static void main(String[] args) {
		
		Counter counter = new Counter(0);
		
		new Pinger(counter).start();
		new Ponger(counter).start();
	
	}

}
