package pcd.lab03.not_sync;

import java.util.concurrent.Semaphore;

public class Pinger extends Thread {

	private Counter counter;
	
	public Pinger(Counter c) {
		counter = c;
	}	
	
	public void run() {
		while (true) {
			try {
				counter.inc();
				System.out.println("ping!"+counter.getValue());
				Thread.sleep(1000);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}