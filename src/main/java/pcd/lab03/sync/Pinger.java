package pcd.lab03.sync;

import java.util.concurrent.Semaphore;

public class Pinger extends Thread {

	private Counter counter;
	private Semaphore isMyTurn;
	private Semaphore isYourTurn;
	
	public Pinger(Counter c, Semaphore isMyTurn, Semaphore isYourTurn) {
		counter = c;
		this.isMyTurn = isMyTurn;
		this.isYourTurn = isYourTurn;
	}	
	
	public void run() {
		while (true) {
			try {
				isMyTurn.acquire();
				counter.inc();
				System.out.println("ping!"+counter.getValue());
				// Thread.sleep(1000);
				isYourTurn.release();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
	}
}